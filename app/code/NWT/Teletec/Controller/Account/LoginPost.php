<?php

namespace NWT\Teletec\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\AreaList;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\State;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Event\Manager as EventManager;
use Magento\Framework\Filesystem;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\ObjectManager\ConfigLoaderInterface;
use Magento\Framework\Registry;
use Magento\Framework\App\Request\Http as Request;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class LoginPost
 * @package NWT\Teletec\Controller\Adminhtml\Account
 */
class LoginPost extends Action
{
    /** @var PageFactory */
    private $pageFactory;

    /** @var FormKeyValidator */
    protected $formKeyValidator;

    /** @var EventManager */
    protected $eventManager;

    /** @var AreaList */
    protected $areaList;

    /** @var ConfigLoaderInterface */
    protected $configLoader;

    /** @var State */
    protected $state;

    /** @var Filesystem */
    protected $filesystem;

    /** @var Registry */
    protected $registry;

    /** @var Request */
    protected $request;

    /** @var ObjectManagerInterface */
    protected $objectManager;

    /** @var EncryptorInterface */
    protected $encryptor;

    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param FormKeyValidator $formKeyValidator
     * @param EventManager $eventManager
     * @param AreaList $areaList
     * @param ConfigLoaderInterface $configLoader
     * @param State $state
     * @param Filesystem $filesystem
     * @param Registry $registry
     * @param Request $request
     * @param ObjectManagerInterface $objectManager
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        FormKeyValidator $formKeyValidator,
        EventManager $eventManager,
        AreaList $areaList,
        ConfigLoaderInterface $configLoader,
        State $state,
        Filesystem $filesystem,
        Registry $registry,
        Request $request,
        ObjectManagerInterface $objectManager,
        EncryptorInterface $encryptor
    ) {
        parent::__construct($context);

        $this->pageFactory = $pageFactory;
        $this->formKeyValidator = $formKeyValidator;
        $this->eventManager = $eventManager;
        $this->areaList = $areaList;
        $this->configLoader = $configLoader;
        $this->state = $state;
        $this->filesystem = $filesystem;
        $this->registry = $registry;
        $this->request = $request;
        $this->objectManager = $objectManager;
        $this->encryptor = $encryptor;
    }

    public function execute()
    {
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            $this->_redirect($this->_redirect->getRefererUrl());
        }

        $params = $this->getRequest()->getParams();
        $username = $params["username"];
        $password = $params["password"];
        $areaCode = 'adminhtml';

        try {
            $this->state->setAreaCode($areaCode);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            // intentionally left empty
        }

        $this->request->setPathInfo('/admin');
        $this->objectManager->configure($this->configLoader->load($areaCode));

        /** @var \Magento\User\Model\User $user */
        $user = $this->objectManager->get('Magento\User\Model\User')->loadByUsername($username);

        if ($this->encryptor->isValidHash($password, $user->getPassword()) === false) {
            $this->_redirect($this->_redirect->getRefererUrl());
        }

        /** @var \Magento\Backend\Model\Auth\Session $session */
        $session = $this->objectManager->get('Magento\Backend\Model\Auth\Session');
        $session->setUser($user);
        $session->processLogin();

        if ($session->isLoggedIn()) {
            $cookieManager = $this->objectManager->get('Magento\Framework\Stdlib\CookieManagerInterface');
            $cookieValue = $session->getSessionId();
            if ($cookieValue) {
                $sessionConfig = $this->objectManager->get('Magento\Backend\Model\Session\AdminConfig');
                $cookiePath = str_replace('autologin.php', 'index.php', $sessionConfig->getCookiePath());
                $cookieMetadata = $this->objectManager->get('Magento\Framework\Stdlib\Cookie\CookieMetadataFactory')
                    ->createPublicCookieMetadata()
                    ->setDuration(3600)
                    ->setPath($cookiePath)
                    ->setDomain($sessionConfig->getCookieDomain())
                    ->setSecure($sessionConfig->getCookieSecure())
                    ->setHttpOnly($sessionConfig->getCookieHttpOnly());
                $cookieManager->setPublicCookie($sessionConfig->getName(), $cookieValue, $cookieMetadata);
                /** @var \Magento\Security\Model\AdminSessionsManager $adminSessionManager */
                $adminSessionManager = $this->objectManager->get('Magento\Security\Model\AdminSessionsManager');
                $adminSessionManager->processLogin();

            }

            /** @var \Magento\Backend\Model\UrlInterface $backendUrl */
            $backendUrl = $this->objectManager->get('Magento\Backend\Model\UrlInterface');
            $path = $backendUrl->getStartupPageUrl();
//            $url = $backendUrl->getUrl($path);
            $url = $backendUrl->getUrl("teletec/teletec/index");
            $url = str_replace('autologin.php', 'index.php', $url);
            header('Location:  '.$url);
            exit;
        }
        return $this->_response;

//        return $this->pageFactory->create();
    }
}
