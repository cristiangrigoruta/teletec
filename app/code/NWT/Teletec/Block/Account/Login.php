<?php

namespace NWT\Teletec\Block\Account;

use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Login
 * @package NWT\Teletec\Block\Account
 */
class Login extends Template
{
    /** @var FormKey */
    protected $formKey;

    /**
     * Login constructor.
     * @param FormKey $formKey
     * @param Context $context
     * @param HttpContext $httpContext
     * @param array $data
     */
    public function __construct(
        FormKey $formKey,
        Context $context,
        HttpContext $httpContext,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->httpContext = $httpContext;
        $this->formKey = $formKey;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getFormAction()
    {
//        return $this->_storeManager->getStore()->getBaseUrl().'teletec/account/loginPost';
        return $this->getUrl('teletec/account/loginPost');
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}