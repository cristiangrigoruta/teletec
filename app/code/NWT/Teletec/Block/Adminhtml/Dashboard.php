<?php

namespace NWT\Teletec\Block\Adminhtml;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as BackendHelper;

/**
 * Class Dashboard
 * @package NWT\Teletec\Block\Adminhtml
 */
class Dashboard extends Template
{
    /** @var BackendHelper */
    protected $backendHelper;

    /**
     * Dashboard constructor.
     * @param BackendHelper $backendHelper
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        BackendHelper $backendHelper,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->backendHelper = $backendHelper;
    }

    /**
     * @return string
     */
    public function getCatalogAdminUrl()
    {
        return $this->backendHelper->getUrl('catalog/product/index');
    }
}