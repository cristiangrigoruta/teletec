<?php

namespace NWT\Teletec\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 * @package NWT\Teletec\Controller
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ? ActionInterface
    {
        $identifier = trim($request->getPathInfo(), '/');

        if ($identifier === "teletec/account/login") {
            $request->setModuleName('teletec');
            $request->setControllerName('account');
            $request->setActionName('login');

            return $this->actionFactory->create(Forward::class, ['request' => $request]);
        }

        if ($identifier === "teletec/account/loginPost") {
            $request->setModuleName('teletec');
            $request->setControllerName('account');
            $request->setActionName('loginPost');

            return $this->actionFactory->create(Forward::class, ['request' => $request]);
        }

        return null;
    }
}
